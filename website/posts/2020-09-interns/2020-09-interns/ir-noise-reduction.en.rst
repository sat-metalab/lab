.. title: Noise reduction for ray tracing acoustics simulation
.. slug: ir-noise-reduction
.. date: 2020-09-24 17:00:00 UTC-04:00
.. tags: internships 
.. category:
.. link: 
.. description: 
.. type: closed

Internship title
----------------
Noise reduction for ray tracing acoustics simulation

Objective
---------

The Metalab develops vaRays, a sound ray launching engine that simulates the acoustic response (or impulse response, IR) of a place according to its geometry and the materials that compose it. The quality of the simulation is directly linked to the number of rays launched and the simulation must be done for each listener / sound source couple, which can lead to a very important amount of calculations. One area for improvement would be to add a noise reduction pass on the IRs in order to keep the number of rays to be sent reasonable. Modern approaches in the field of visual rendering implement deep learning with excellent results.

The objective of this course is therefore to explore the application of deep learning to reduce IR noise and improve the quality of the simulation with a given number of rays launched.

Tasks
-----
With the help of the Metalab team:

* get acquainted with the Python API of vaRays
* make a state of the art of noise reduction methods for IRs
* make a state of the art of noise reduction methods for visual rendering, and evaluate the transposition to acoustic simulation
* implement and experiment noise reduction methods for IRs


Work environment
----------------
* Python, Tensorflow, PyTorch
* spatial audio, ambisonics & acoustics
* Gitlab, JIRA / Confluence
* Linux
* https://sat-metalab.gitlab.io/varays
* https://www.openimagedenoise.org/
