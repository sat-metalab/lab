.. title: Implementation of Free and Open Source Software (FOSS) community animation tools
.. slug: foss-animation
.. date: 2020-09-24 17:00:00 UTC-04:00
.. tags: internships 
.. category:
.. link: 
.. description: 
.. type: closed

Internship title
----------------
Implementation of Free and Open Source Software (FOSS) community animation tools

Objective
---------
Set up an infrastructure allowing contributors of Shmdata and ndi2shmdata open source software to plan, document, communicate and participate in the decision-making process of Shmdata evolutions. 

Tasks
-----
* Generate a website in the GitLab CI from existing documentation
* Migration of tasks from the private system (JIRA) to the GitLab system
* Enriching the website with contribution and communication processes
* Monitor the chat systems used by others
* Possible continuous integration improvements to support multiple Linux distributions (compilation, packaging, versioning)
* Possible creation of GitLab repo dedicated to a coherent echosystem (Shmdata, ndi2shmdata, Switcher & Scenic)
* Participate in the life of the laboratory: scrums, code review, etc.
* Document the work and ensure its reproducibility.

Work environment
----------------
* GitLab, python, markdown, Bash
* Linux, free tools
* https://gitlab.com/sat-metalab/shmdata/
* https://gitlab.com/sat-metalab/ndi2shmdata/
* https://gitlab.com/sat-metalab/switcher
* https://sat-mtl.gitlab.io/telepresence/scenic/
