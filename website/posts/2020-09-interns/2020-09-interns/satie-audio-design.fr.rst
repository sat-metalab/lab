.. title: Design audio pour environnement immersif
.. slug: design-audio-pour-environnement-immersif
.. date: 2020-09-24 17:00:00 UTC-04:00
.. tags: stages 
.. category:
.. link: 
.. description: 
.. type: closed


Titre du stage
--------------
Design audio avec SATIE pour environnements immersifs

Objectif
--------
SATIE est un logiciel conçu pour la spatialisation sonore pour les configurations des haut-parleurs hétérogènes. Fait à la base de logiciel de programmation audio SuperCollider, il est aussi adapté à la génération et traitement des sons. Ce stage a pour but de bonifier sa bibliothèque des sons synthétiques et effets utiles pour les démonstrations ou pour inspirer les productions. En même temps trouver des pipelines de production qui permettent une coopération des outils arbitraires avec SATIE.

Tâches
------
Avec le soutien de l'équipe du Metalab :

* Participer à l'idéation
* Proposer et développer des "plugins" de synthèse et effets pour SATIE accompagnés des courts exemples
* En collaboration avec l'équipe du Metalab et l'équipe de production, identifier les possibilités de pipelines de production flexibles
* Participer au développement d'un démo (vidéo ou live)
* Participer à la vie du laboratoire : scrums, code review, etc.
* Documenter le travail et s'assurer de sa reproductibilité

Environnement de travail
------------------------
* Scrums, code review, etc.
* Linux, outils libres
* SuperCollider, git, JACK, écosystème audio sous Linux
* Potentiellement Bash, Python
