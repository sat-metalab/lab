.. title: Génération automatique de coordonnées de texture pour le projection mapping
.. slug: coordonnees-de-texture
.. date: 2020-09-24 17:00:00 UTC-04:00
.. tags: stages 
.. category:
.. link: 
.. description: 
.. type: closed

Titre du stage
--------------
Génération automatique de coordonnées de texture pour le projection mapping

Objectif
--------
Les installations de projection mapping requièrent que le contenu diffusé concorde avec l'environnement physique. C'est la phase de calibrage des dispositfs d'affichage qui permet aux projections vidéo d'être placées précisément sur la surface immersive. L'objectif de ce stage concerne une étape essentiel du processus de calibrage. Il s'agit de développer un prototype qui à partir d'un maillage d'une surface de projection calcule des coordonnées de texture en fonction d'un point de vue utilisateur.

Tâches
------
Avec le soutien de l'équipe du Metalab:

* Exploration des techniques de génération de coordonnées de texture pour notre cas d'usage
* Travailler avec des nuages de point représentant des surfaces de projection telle un dôme, un demi-dôme, un plan, ou autres géométries asbtraites
* Développer et évaluer des prototypes
* Participer à la vie du laboratoire : scrums, révision de code, etc.
* Documenter le travail et s'assurer de sa reproductibilité
* Participer à la vie du laboratoire : scrums, code review, etc.
* Documenter le travail et s'assurer de sa reproductibilité

Environnement de travail
------------------------
* Scrums, code review, etc.
* C++ / Python
* JIRA / Confluence
* GitLab
* Linux, Free and Open Source Software (FOSS)
