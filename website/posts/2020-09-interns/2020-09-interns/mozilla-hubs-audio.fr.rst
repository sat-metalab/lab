.. title: Modification du système de rendu audio de Mozilla Hubs
.. slug: mozilla-hubs-audio
.. date: 2020-09-24 17:00:00 UTC-04:00
.. tags: stages 
.. category:
.. link: 
.. description: 
.. type: closed


Titre du Stage
--------------
Modification du système de rendu audio de Mozilla Hubs, un environnement pour de réalité virtuelle sociale dans le navigateur.

Objectif
--------
Analyser la logique de transmission audio de Mozilla Hubs et évaluer les possibilité de modification de son moteur de rendu audio spatialisé.

Tâches
------
Avec le soutien de l'équipe du Metalab :

* Produire une analyse du code et du comportement de la plateforme libre Mozilla Hubs
* Proposer des approches possibles de modification et remplacement du système de spatialisation de Mozilla Hubs
* Prototyper des modifications du moteur de spatialisation
* Participer à la production une démonstration vidéo pour la fin du stage
* Participer à la vie du laboratoire : scrums, code review, etc.
* Documenter le travail et s'assurer de sa reproductibilité

Environnement de travail
------------------------
* Scrums, code review, etc.
* Mozilla Hubs : https://hubs.mozilla.com/
* Javascript, nodeJS, Amazon AWS, git
* GitLab, python, Bash
* Linux, outils libres
