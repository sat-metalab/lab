.. title: Mise en place d'outil d'animation de communauté Free et Open Source Software
.. slug: animation-foss
.. date: 2020-09-24 17:00:00 UTC-04:00
.. tags: stages 
.. category:
.. link: 
.. description: 
.. type: closed


Titre du Stage
--------------
Mise en place d'outil d'animation de communauté Free et Open Source Software (FOSS)

Objectif
--------
Mettre en place une infrastructure permettant aux contributeurs des logiciels libres Shmdata et ndi2shmdata de planifier, documenter, communiquer et de participer au processus de décision des évolutions de Shmdata. 

Tâches
------
Avec le soutien de l'équipe du Metalab :

* Générer un site web dans le CI de GitLab à partir de la documentation existante
* Migration des taches du système privé (JIRA) vers le système GitLab
* Enrichir le site web avec les processus de contribution et de communication
* Effectuer une veille sur les système de clavardage utilisé par d'autres
* Possiblement amélioration de l’intégration continue pour le support de multiples distributions Linux (compilation, packaging, version)
* Possiblement création de repo GitLab dédié à un échosystème cohérent (Shmdata, ndi2shmdata, Switcher & Scenic)
* Participer à la vie du laboratoire : scrums, code review, etc.
* Documenter le travail et s'assurer de sa reproductibilité

Environnement de travail
------------------------
* Scrums, code review, etc.
* GitLab, python, markdown, Bash
* Linux, outils libres
* https://gitlab.com/sat-metalab/shmdata/
* https://gitlab.com/sat-metalab/ndi2shmdata/
* https://gitlab.com/sat-metalab/switcher
* https://sat-mtl.gitlab.io/telepresence/scenic/
