.. title: SATIE - Web design and integration
.. slug: web-design-and-integration
.. date: 2020-09-24 15:00:00 UTC-04:00
.. tags: internships
.. category:
.. link: 
.. description: 
.. type: closed

Internship title
----------------
User experience design and integration for web application dedicated to sound spatialization.

Objective
---------
Participate in the design and improvement of the user experience of the web interface of SATIE, our audio spatialization software.

Tasks
-----
With the help of the Metalab team:

* Analyze the experience of speaker system calibration of the current interface for headphones and other custom speaker systems
* Propose one or more interface models
* Possibly implement the interface into the existing tool
* Possibly other user experience improvements
* Participate in the life of the laboratory: scrums, code review, etc.
* Document the work and ensure its reproducibility.

Work environment
----------------
* React, SocketIO, Node.js, Javascript, Python, SuperCollider, OSC, Git
* GitLab, Bash, Linux, open source tools
* https://gitlab.com/sat-metalab/poire
* https://gitlab.com/sat-metalab/SATIE
* https://vimeo.com/265607999
* https://vimeo.com/290925507
