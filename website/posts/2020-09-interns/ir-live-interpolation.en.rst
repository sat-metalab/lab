.. title: Real-time interpolation of impulse responses
.. slug: ir-live-interpolation
.. date: 2020-09-24 17:00:00 UTC-04:00
.. tags: internships 
.. category:
.. link: 
.. description: 
.. type: closed

Internship title
----------------
Real-time interpolation of impulse responses

Objective
---------
   
Immersive audio has been a hot topic of research for the past decade or so with the resurrection of Virtual/Augemented reality and immersive media. Accurate rendering of acoustics in a virtual environment in real-time is as essential as real-time graphic rendering to achieve a true sense of immersion in a virtual space since we rely on auditory cues as much as visual cues to get an estimation of the size of our surroundings. This cues include, and are not limited to, the direction of arrival of acoustic waves and their reflections from the inanimate objects in the environment such as walls, floor, ceiling, furniture, etc.  This internship will focus on applying 3D room impulse responses measured in real-world environments like concert halls to recreate experiences in virtual immersive environments along with acoustic simulation techniques like acoustic ray tracing.

Tasks
-----
* Explore interpolation techniques to make use of recorded ambisonic room impulse responses in navigable virtual spaces.
* Work with room impulse response datasets and perform analyses.
* Develop prototypes and document them for reproduction of results.

Work environment
----------------
* Digital signal processing, numpy
* Python, C++.
* 3D audio and ambisonics.
* VR/AR
