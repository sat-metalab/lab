.. title: Improvement of the colorimetric chain of our projection mapping tool (Splash)
.. slug: colorimetric-chain-splash
.. date: 2022-01-10
.. tags: internships 
.. category:
.. link: 
.. description: 
.. type: filled
.. author: Emmanuel Durand
.. intern: Éva Décorps
.. mentors: Christian Frisson, Emmanuel Durand


Internship title
----------------
Improvement of the colorimetric chain of our projection mapping tool (Splash)

Objective
---------
The visual quality of a projection system involving several video projectors, using projection mapping techniques, can lead to visible differences between projectors despite excellent geometric calibration. This can be due to the projection surface, physical differences between the projectors, variations in their installations, etc. In addition, projection mapping devices generally show weaknesses when it comes to the projection of dark content because the projectors do not project complete black. The overlapping areas between projections then become clearly visible, which reduces the visual quality and possibly the immersion of the user.

* Ensure the coherence of the colorimetric chain of Splash, and possibly to improve it
* Improve the handling of colorimetry and to implement modern colorimetric calibration methods in Splash
* Improve the support of blending areas (blending) in particular by implementing methods of color diffusion (or dithering) in the case of a variable setting of the black level.

Tasks
-----
With the help of the Metalab team:

* Get acquainted with the Splash code
* Take part in the documentation of the operation of the colorimetric chain of Splash
* Experiment the colorimetric calibration as existing, to propose and implement improvements
* Experiment with overlay zone support for dark content, propose and implement enhancements

Work environment
----------------
* C++ / OpenGL / OpenCV
* JIRA / Confluence
* GitLab
* Linux, Free and Open Source Software (FOSS)
* https://sat-metalab.gitlab.io/splash
* https://en.wikipedia.org/wiki/Black_level
* https://en.wikipedia.org/wiki/Dither
* https://www.arnoldrenderer.com/research/dither_abstract.pdf
