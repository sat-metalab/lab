.. title: Shadow removal in a projection mapping context
.. slug: shadow-removal-in-a-projection-mapping-context
.. date: 2022-10-11
.. tags: internships 
.. category:
.. link: 
.. description: 
.. type: text

Internship title
----------------
Shadow removal for projection mapping

Objective
---------
Projection mapping installations dedicated to the setting up of visual immersion spaces are subject to unwanted shadows cast by the users of these spaces, during the passage between the projector and the projection surface. However, shadow suppression methods are possible when the projections overlap sufficiently (see https://www.youtube.com/watch?v=_SREVKe8QhQ). The objective would be to develop and implement a shadow suppression method in the Splash mapping projection software (https://gitlab.com/sat-metalab/splash).

Tasks
-----
* Produce a state of the art about shadow removal
* Propose a method adapted to our uses
* Participate in the implementation of the chosen method in Splash
* Participate in the creation of a demonstration video
* Participate in the life of the laboratory: scrums, code review, etc.
* Document the work and ensure its reproducibility.

Work environment
----------------
* C++, Python
* OpenGL, GLSL, OpenCV
* Linux, Free and Open Source Software (FOSS)
