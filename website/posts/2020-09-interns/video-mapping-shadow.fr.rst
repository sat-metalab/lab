.. title: Suppression des ombres dans un contexte de projection mapping
.. slug: ombres-projection-mapping
.. date: 2022-10-11
.. tags: stages 
.. category:
.. link: 
.. description: 
.. type: text


Titre du stage
--------------
Suppression des ombres dans un contexte de projection mapping

Objectif
--------
Les installations de projection mapping dédiées à la mise en place d'espaces d'immersion visuelle sont sujettes aux ombres portées par les usagers de ces espaces, lors du passage entre le projecteur et la surface de projection. Des méthodes de suppression d'ombre sont cependant envisageables lorsque les projections se recouvrent suffisamment (voir https://www.youtube.com/watch?v=_SREVKe8QhQ). L'objectif serait de mettre au point et d'implémenter une méthode de suppression des ombres dans le logiciel de projection mapping Splash (https://gitlab.com/sat-metalab/splash)

Tâches
------
Avec le soutien de l'équipe du Metalab :

* Faire un état de l'art des méthodes de suppression des ombres
* Proposer une méthode adaptée à nos usages
* Participer à l'implémentation de la méthode choisie dans Splash
* Participer à la création d'une vidéo de démonstration
* Participer à la vie du laboratoire : scrums, code review, etc.
* Documenter le travail et s'assurer de sa reproductibilité

Environnement de travail
------------------------
* Scrums, code review, etc.
* C++, Python
* OpenGL, GLSL, OpenCV
* Linux, Free and Open Source Software (FOSS)
