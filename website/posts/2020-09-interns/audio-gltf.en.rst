.. title: Audio extension for the 3D scene exchange format glTF
.. slug: 
.. date: 2022-10-11
.. tags: internships 
.. category:
.. link: 
.. description: 
.. type: text

Internship title
----------------
Definition and implementation of an audio extension for the 3D scene exchange format glTF

Objective
---------
The glTF format is a standardized standard (https://github.com/KhronosGroup/glTF) allowing the exchange of complex 3D scenes including 3D models, animations, cameras, etc. However, this format does not support the audio part. It is therefore necessary to define a dedicated extension to describe the sound aspects of a 3D scene.

Tasks
-----
* Make a state of the art of audio scene description formats
* Participate in the definition of the specifications of an audio extension in glTF format
* Develop an implementation example
* Add support for the extension in the tools developed/used by the Metalab: EiS, vaRays, Blender
* Participate in the life of the laboratory: scrums, code review, etc.
* Document the work and ensure its reproducibility.

Work environment
----------------
* Blender: https://blender.org
* Linux, Python, C++, Free and Open Source Software (FOSS)
