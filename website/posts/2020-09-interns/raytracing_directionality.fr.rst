.. title: Ajout de directionalité des sources dans notre outil de lancé de rayons audio (vaRays)
.. slug: directionalité_vaRays
.. date: 2022-10-11
.. tags: stages
.. category:
.. link: 
.. description: 
.. type: text

Titre du stage
--------------
Ajout de directionalité des sources dans notre outil de lancé de rayons audio (vaRays)

Objectif
--------
Notre engin de simulation audio, vaRays, utilise du lancé de rayons (ray tracing) pour simuler l'acoustique de millieux virtuels. L'objectif du stage sera de proposer et d'implémenter une méthode pour permettre différentes distributions d'émission de l'énergie des sources sonores dans vaRays.

Tâches
------
Avec le soutien de l'équipe du Metalab :

* Prendre connaissance du code de vaRays
* Proposer et expérimenter différentes méthodes d'implémentation de directionalité des sources sonores
* Participer à la création de démonstration de ces implémentations
* Documenter le travail et s'assurer de sa reproductibilité

Environnement de travail
------------------------
* Scrums, code review, etc.
* C++
* GitLab, Linux, Free and Open Source Software (FOSS)
* JIRA / Confluence

