.. title: Re-enforcing continuous integration in gitlab
.. slug: Re-inforcing-CI-integration-in-gitlab
.. date: 2021-03-11 17:00:00 UTC-04:00
.. tags: internships
.. category:
.. link: 
.. description: 
.. type: closed

Internship title
----------------
Re-enforcing continuous integration of Free and Open Source Software (FOSS) developped at the Metalab

Objective
---------
As we want to offer more support for our tools, we need to implement this support in our CI in Gitlab.
Multiple directions are possible:
* Refine software packaging for better distribution
* Support building for ARM architecture in CI (currently only x86)
* Augment test coverage (Python, C++)
* Investigate testing ARM architecture in CI

Tasks
-----
With the help of the Metalab team:
* Identify areas where tests / packaging / cross compilation are missing
* Implement new tests
* Implement cross-compilation on CI
* Implement / refine packaging

Work environment
----------------
* GitLab, Linux, Free and Open Source Software (FOSS)
* C++ / Python / YAML
* JIRA / Confluence
* Docker, QEMU
* NVidia Jetson, Raspberry Pi

