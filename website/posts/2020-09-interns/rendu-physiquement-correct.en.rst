.. title: Development of physically correct rendering in our immersive editing tool (EiS)
.. slug: physically-correct-rendering
.. date: 2020-09-24 17:00:00 UTC-04:00
.. tags: internships 
.. category:
.. link: 
.. description: 
.. type: closed

Internship title
----------------
Development of physically correct rendering in our immersive editing tool (EiS)

Objective
---------
As part of its research, Metalab is developing an immersive editing tool dedicated to facilitating the creation of content for multi-user immersive spaces such as immersive domes: EiS (https://gitlab.com/sat-metalab/EditionInSitu). To improve the integration of this software with the other tools of Metalab, and in particular Blender, we want to improve the rendering of the 3D engine used and add the support of physically based rendering (PBR).

Tasks
-----
* State of the art of the best approaches to add PBR support
* Take in hand the 3D rendering part of EiS
* Determine and implement an appropriate method for PBR rendering in EiS
* Participate in the life of the laboratory: scrums, code review, etc.
* Document the work and ensure its reproducibility.

Work environment
----------------
* Python3
* OpenGL, GLSL, Panda3D (panda3d.org/)
* Linux, Free and Open Source Software (FOSS)
