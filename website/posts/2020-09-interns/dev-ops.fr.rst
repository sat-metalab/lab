.. title: Renforcer l'intégration continue dans gitlab
.. slug: Renforcer l'intégration continue dans gitlab
.. date: 2021-03-11 17:00:00 UTC-04:00
.. tags: stages
.. category:
.. link: 
.. description: 
.. type: closed

Titre du stage
--------------
Renforcer l'intégration continue des logiciels libres développés au sein du Metalab. (devOps)

Objectif
--------
Le Metalab souhaite offrir un plus grand support pour ses outils libres et open source (FOSS: Free Open Source Software).
Ceci sera fait en intégrant différentes solutions dans notre CI Gitlab.
Plusieurs directions sont envisageables:
* Améliorer le packaging de nos logiciels pour faciliter la distribution
* Ajouter une cible ARM dans Gitlab (cross compilation pour Jetson et Raspberry Pi)
* Augmenter la couverture de tests (Python et C++)
* Investiguer comment tester une architecture ARM dans le CI

Tâches
-----
Avec l'aide de l'équipe du Métalab:
* Identifier les endroits où manquent des tests / le packaging / la cross compilation
* Implémenter de nouveau tests
* Implémenter la cross-compilation dans CI
* Implémenter / améliorer le packaging

Environnement de travail
------------------------
* GitLab, Linux, Free and Open Source Software (FOSS)
* C++ / Python / YAML
* JIRA / Confluence
* Docker, QEMU
* NVidia Jetson, Raspberry Pi

