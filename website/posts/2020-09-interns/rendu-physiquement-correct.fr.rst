.. title: Développement du rendu physiquement correct dans notre outil d'édition immersive (EiS)
.. slug: rendu-physiquement-correct
.. date: 2020-09-24 17:00:00 UTC-04:00
.. tags: stages 
.. category:
.. link: 
.. description: 
.. type: closed


Titre du stage
--------------
Développement du rendu physiquement correct dans notre outil d'édition immersive (EiS)

Objectif
--------
Dans le cadre de ses recherches, le Metalab développe un outil d'édition immersive dédié à faciliter la création de contenu pour les espaces immersifs multi-utilisateurs tels que les dômes immersifs : EiS (https://gitlab.com/sat-metalab/EditionInSitu). Pour améliorer l'intégration de ce logiciel avec les autres outils du Metalab, et en particulier Blender, nous souhaitons améliorer le rendu du moteur 3D utilisé et y ajouter le support du rendu physiquement correct (ou PBR pour physically based  rendering).

Tâches
------
Avec le soutien de l'équipe du Metalab :

* Faire l'état de l'art des meilleures approches pour ajouter le support du PBR
* Prendre en main la partie rendu 3D de EiS
* Déterminer et implémenter une méthode appropriée pour le rendu PBR dans EiS
* Participer à la vie du laboratoire : scrums, code review, etc.
* Documenter le travail et s'assurer de sa reproductibilité

Environnement de travail
------------------------
* Scrums, code review, etc.
* Python3
* OpenGL, GLSL, Panda3D (panda3d.org/)
* Linux, Free and Open Source Software (FOSS)
