.. title: Interpolation en temps réel de réponses impultionnelles
.. slug: interpolation-en-temps-reel-de-reponses-impultionnelles
.. date: 2020-09-24 17:00:00 UTC-04:00
.. tags: stages 
.. category:
.. link: 
.. description: 
.. type: closed

Titre du stage
--------------
Interpolation en temps réel de réponses impulsionnelles

Description
-----------

L'audio immersif est un sujet de recherche brûlant depuis une dizaine d'années avec la résurrection de la réalité virtuelle/augmentée et des médias immersifs. Le rendu précis de l'acoustique dans un environnement virtuel en temps réel est aussi essentiel que le rendu graphique en temps réel pour obtenir un véritable sentiment d'immersion dans un espace virtuel puisque nous nous appuyons sur des indices auditifs autant que visuels pour obtenir une estimation de la taille de notre environnement. Ces indices comprennent, sans s'y limiter, la direction d'arrivée des ondes acoustiques et leurs réflexions sur les objets inanimés présents dans l'environnement, tels que les murs, le sol, le plafond, les meubles, etc.  Ce stage se concentrera sur l'application de réponses impulsionnelles de salle en 3D mesurées dans des environnements réels comme des salles de concert pour recréer des expériences dans des environnements immersifs virtuels, ainsi que sur des techniques de simulation acoustique comme le ray tracing acoustique.

Tâches
------
Avec le soutien de l'équipe du Metalab :

* Explorer les techniques d'interpolation pour utiliser les réponses impulsionnelles enregistrées dans les pièces ambisoniques dans les espaces virtuels navigables.
* Travailler avec des ensembles de données sur les réponses impulsionnelles des pièces et effectuer des analyses.
* Développer des prototypes et les documenter pour la reproduction des résultats.
* Participer à la vie du laboratoire : scrums, code review, etc.
* Documenter le travail et s'assurer de sa reproductibilité

Environnement de travail
------------------------
* Scrums, code review, etc.
* Traitement numérique des signaux.
* Python, C++.
* Audio 3D, audio ambisonique.
* Développement en réalité virtuelle et augmentée
