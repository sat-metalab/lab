<!--
.. title: IR, Calibration et Visualisation
.. slug: ir-calibration-and-visualisation
.. date: 2022-10-11
.. tags: stages
.. category:
.. link:
.. description:
.. type: text
-->

# Titre du stage
Réponse impulsionnelle, calibrage et visualisation des signaux audio

# Objectif

Ce stage vise à poursuivre le développement de recettes, d'approches et d'outils autour de l'enregistrement et de la visualisation de la réponse impulsionnelle. Le but de ce travail est d'identifier certaines propriétés acoustiques d'un espace physique pour aider à la calibration de l'équipement audio.

# Tâches

- Participer à l'idéation
- Explorer les outils existants, internes et tiers, pour des tâches similaires.
- Expérimenter et développer ou améliorer les outils internes existants ou les pipelines impliquant d'autres outils.
- Participer à l'élaboration d'une démo (vidéo ou live)
- Participer à la vie du laboratoire : scrums, revue de code, etc.
- Documenter le travail et assurer sa reproductibilité.

# Contexte et logiciels

- Linux OS
- Traitement du signal numérique
- Python (numpy), bash
- Audio 3D et ambisoniques
- VR/AR
- Facultatif : SuperCollider

# Contact

Envoyer un CV à metalab-stage@sat.qc.ca
