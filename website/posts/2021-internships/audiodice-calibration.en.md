<!--
.. title: Audiodice speaker calibration
.. slug: audiodice-calibration
.. date: 2022-04-25
.. tags: internships
.. category: audio
.. link:
.. description:
.. type: filled
.. author: Thomas Piquet
.. intern: Pierre Gilbert
.. mentors: Thomas Piquet, Nicolas Bouillot
-->

# Internship title
Audiodice calibration

# Objective
This interships aims to calibrate audiodice speakers, a set of 5 dodecahedron with one speaker per side.

https://vimeo.com/519938720

This type of speaker needs to be calibrated in two ways:
- Frequency response
- Directivity of the speaker depending it's configuration (single speaker at once, omnidirectional pattern)

From this calibration data, we will be able to create a profile of the audiodice and use it to enhance the sound reproduction. It will also help to create audio spatializer thanks to the directivity knowledge.


# Tasks

- Participate in the measurements
- Explore existing in-house and other tools for similar tasks
- Experiment and develop or improve existing in-house tools or pipelines involving other tools
- Participate in the development of a demo (video or live)
- Participate in the life of the laboratory: scrums, code review, etc.
- Document the work and ensure its reproducibility.

# Context and software

- Linux OS
- Digital signal processing
- Python (numpy), bash
- 3D audio and ambisonics
- Optional: SuperCollider
