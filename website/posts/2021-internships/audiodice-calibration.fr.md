<!--
.. title: Calibration des haut-parleurs audiodices
.. slug: audiodice-calibration
.. date: 2022-04-25
.. tags: stages
.. category: audio
.. link:
.. description:
.. type: filled
.. author: Thomas Piquet
.. intern: Pierre Gilbert
.. mentors: Thomas Piquet, Nicolas Bouillot
-->

# Titre du stage
Calibration des audiodices

# Objectif
Ce stage vise à la calibration des audiodices, un ensemble de 5 enceintes dodécaédrique avec un haut-parleur par face.

https://vimeo.com/519938720

Ce type d'enceinte nécessite une calibration sur deux points:
- La réponse en fréquence
- La directivité de l'enceinte en fonction de sa configuration (un seul haut-parleur à la fois, mode omnidirectionnel)

A partir de ces informations de calibration, nous serons capables de créer un profil des audiodices pour améliorer la reproduction sonore. Ces informations nous aiderons aussi de développer un spatialiseur audio.

# Tâches

- Participer aux mesures.
- Explorer les outils existants, internes et tiers, pour des tâches similaires.
- Expérimenter et développer ou améliorer les outils internes existants ou les pipelines impliquant d'autres outils.
- Participer à l'élaboration d'une démo (vidéo ou live)
- Participer à la vie du laboratoire : scrums, revue de code, etc.
- Documenter le travail et assurer sa reproductibilité.

# Contexte et logiciels

- Linux OS
- Traitement du signal numérique
- Python (numpy), bash
- Audio 3D et ambisoniques
- Facultatif : SuperCollider
