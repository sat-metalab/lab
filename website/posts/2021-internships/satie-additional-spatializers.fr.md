<!--
.. title: Spatialisateurs supplémentaires pour SATIE
.. slug: satie-additional-spatializers
.. date: 2022-10-11
.. tags: stages
.. category: audio
.. link:
.. description:
.. type: text
-->

# Titre du stage
Types de spatialisateurs supplémentaires pour SATIE

# Objectif

SATIE est notre spatialisateur audio interne qui se veut flexible, robuste et qui supporte de nombreuses configurations de haut-parleurs. Ce stage vise à apporter de nouvelles techniques de spatialisation à SATIE.

# Tâches

- Participer à l'idéation
- Passer en revue la littérature qui documente et discute des techniques de spatialisation.
- Développer et tester des plugins de spatialisation pour SATIE (en utilisant le framework SuperCollider)
- Participer au développement d'une démo (vidéo ou live)
- Participer à la vie du laboratoire : scrums, revue de code, etc.
- Documenter le travail et assurer sa reproductibilité.

# Contexte et logiciels

- Linux OS
- Traitement du signal numérique
- SuperCollider, Bash
- audio 3D et ambisonique
- VR/AR
- En option : Python

# Contact

Envoyer un CV à metalab-stage@sat.qc.ca
