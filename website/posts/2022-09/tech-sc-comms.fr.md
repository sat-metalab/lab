<!--
.. title: Communications scientifiques grand public : immersion collective
.. slug: comms-tech-sc
.. date: 2022-10-11
.. tags: stages, vulgarisation, communications
.. category: communications
.. link:
.. description:
.. type: tbc
.. author: Metalab
-->

# Titre du stage

Stratégies pour améliorer les communications scientifiques grand public du Metalab au sujet de l'immersion collective.

*Ce stage est une [opportunité pour un projet Mitacs](https://www.mitacs.ca/fr/opportunity/6526/view)*

# Contexte

Formé depuis 2002, le Metalab est le laboratoire de recherche de la Société des Arts Technologiques [[SAT](https://sat.qc.ca/fr/recherche/metalab)]. 
La mission du [Metalab](https://sat-mtl.gitlab.io) est double: 

1. stimuler l'émergence d'expériences immersives innovantes, 
2. rendre leur conception accessible aux artistes et aux créateurs de l'immersion à travers un écosystème de logiciels libres.

En plus de notre ensemble d'outils pour le rendu audio et visuel, nous explorons des pistes pour développer des expériences immersives dans des lieux non-dédiés. Afin d'améliorer la compréhension des travaux du Metalab par le grand public et de favoriser l'adoption de ses outils, nous souhaitons travailler sur un plan stratégique de communications scientifiques et techniques.

# Objectif

Ce stage vise à décrire un premier plan stratégique de communications scientifiques grand public et d'en commencer la mise en application.

Les types de communications considérées pour ce stage incluent mais ne se limitent pas à :

* des billets de blogue;
* des publications sur Twitter;
* des présentations publiques;
* des vidéos sur Internet;
* des entrevues;
* des articles dans des revues de vulgarisation scientifique.

Nous souhaitons faire connaître nos travaux et les questions de recherche du Metalab ainsi que les enjeux reliés de façon à les rendre accessibles à la population.

# Tâches

- Effectuer une revue de presse des communications grand public actuelles du Metalab
- Établir un état des lieux des meilleurs pratiques en vulgarisation scientifique
- Faire une liste d'actions possibles pour le Metalab
- Réalisation d'au moins une communication grand public avec le soutien de l'équipe

# Environnement de travail

- Documentation: BibTeX, LaTeX, Markdown, reStructuredText, Sphinx
- Processus: Confluence, Gitlab
- Ecosystème: Linux, Logiciels Libres (FOSS)

# Références

- [compte Twitter sat_metalab](https://www.twitter.com/sat_metalab)
- [blogue du Metalab](https://sat-mtl.gitlab.io/metalab/blog)
