<!--
.. title: Scientific communications for the general public: collective immersion
.. slug: comms-tech-sc
.. date: 2022-10-11
.. tags: internships, science journalism, communications
.. category: communications
.. link:
.. description:
.. type: tbc
.. author: Metalab
-->

# Internship title

Strategies to improve scientific communications for the general public at Metalab on the topic of collective immersion.

*This internship is a [Mitacs project opportunity](https://www.mitacs.ca/en/opportunity/6526/view)*

# Context

FSince 2002, the Metalab is the Society for Arts and Technology research and development lab [[SAT](https://sat.qc.ca/fr/recherche/metalab)]. 
The [Metalab](https://sat-mtl.gitlab.io) has a dual mission:

1. to stimulate the emergence of new immersive experiences
2. to make their design and authoring accessible to artists and creators through an ecosystem of free software.

We are currently exploring possible paths to improve the development of immersive experiences in spaces that were not conceived for immersion, as a continuation of our work on toolkits for visual and audio rendering.

To improve the general public understanding of our work and to help onboard new users into using our toolset, we wish to work on a strategy for technical and scientific communications.

# Goal

The goal of this internship is to design a first communications plan for strategic communications for the general public, and to start implementing the plan.

Possible types of communications includes but are not restricted to:

* blogposts
* Twitter posts
* public speaking
* videos published on the Internet
* interviews
* articles in popular science magazines.

We would like to promote our work and the research questions we explore at Metalab, as well as the related stakes in a way that makes them accessible to the general public.

# Tasks

- Perform a review of the current general public communications of Metalab
- Research about the state of the art of best practices for general public scientific communications
- Establish a list of possible actions for the Metalab
- With the support of the team: do at least one general public communication

# Work environment

- Documentation: BibTeX, LaTeX, Markdown, reStructuredText, Sphinx
- Process: Confluence, Gitlab
- Software ecosystem: Linux, Logiciels Libres (FOSS)

# Resources

- [on Twitter: sat_metalab](https://www.twitter.com/sat_metalab)
- [Metalab's blog](https://sat-mtl.gitlab.io/metalab/blog)
