.. title: Linux C++/Python Programming for Technological Arts
.. slug: prog-linux-cpp-python
.. date: 2020-10-21 17:00:00 UTC-04:00
.. tags: jobs 
.. category:
.. link: 
.. description: 
.. type: closed

Position
--------

The Society for Arts and Technology [SAT] needs you for an immediate start at Metalab, the SAT research laboratory. Your role will be to participate in the research process: technology watch, ideation, development, implementation, communication and participation in partnership projects. You will participate in the evolution, code review and use of Metalab software. An experience of at least 5 years is desired.


Technical environment
---------------------

* Ubuntu, Linux, FOSS
* Gcc, python3, cmake
* GDB, clang
* Raspberry Pi, NVidia jetson
* Gitlab CI, Docker, AWS, Azure
* Gitlab, JIRA, Confluence, Slack, Mozilla Hubs

Assets
------
* Real-time audio-visual
* TCP/IP
* Cross compilation
* Kubernetes

Work environment
----------------

Since 2002, the `Metalab <https://sat.qc.ca/fr/recherche/metalab>`__ is the Society for Arts and Technology [`SAT <https://sat.qc.ca/>`_]’s research and development lab. The Metalab has a dual mission: to stimulate the emergence of new immersive experiences, and to make their design and authoring accessible to artists and creators through an ecosystem of free software.

The Metalabs research fields – telepresence, immersion, video mapping, and spatialized sound – are explored through research and production projects. The Metalab team is comprised of 8 to 15 people with various technical expertise including digital audio, computer graphics, networks, software development and multimedia integration. The Metalab welcomes interns in a variety of subjects, even outside the strict scope of the laboratory expertises.

The Metalab's works aims to cover the whole pipeline of immersive and distributed content production. The Metalab’s software addresses, among others, immersive in situ editing, video mapping, audio spatialization, group interaction and low latency transmission. They are designed as flexible tools, and supports Metalab’s research and partnerships. This software suite is used by the SAT during creative residencies and is at the heart of telepresence projects, between libraries (`Bibliolab <https://sat.qc.ca/fr/bibliolab>`_) and between venues (`Scènes ouvertes <https://sat.qc.ca/fr/scenes-ouvertes>`_). Metalab’s partners include artists, university researchers, schools, designers and private companies.

Metalab Software
--------------------

* Immersive Space Editing and Prototyping: `In situ editing (EiS) <https://gitlab.com/sat-metalab/EditionInSitu>`_
* Multi-projector video mapping for all types of surfaces: `Splash <https://gitlab.com/sat-metalab/splash/-/wikis/home>`_
* Calibration of projectors for immersive space: `Calimiro <https://gitlab.com/sat-metalab/calimiro>`_
* Sound spatialization for all types of speaker systems: `SATIE <https://gitlab.com/sat-metalab/SATIE>`_
* Sound spatialization by acoustic simulation: `vaRays <https://gitlab.com/sat-metalab/varays>`_
* Group interaction of people in an immersive space: `Spook <https://gitlab.com/sat-metalab/spook>`_
* Telepresence and multichannel transmission in low latency: `Switcher <https://gitlab.com/sat-metalab/switcher>`_
* Sharing of all types of data streams between applications: `Shmdata <https://gitlab.com/sat-metalab/shmdata>`_
  
