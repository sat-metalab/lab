<!--
.. title: Rédaction technique
.. slug: documentation
.. date: 2022-02-16 15:00:00 UTC-04:00
.. tags: stages 
.. category: documentation
.. link: 
.. description: 
.. type: closed
-->

# Titre du Stage

Rédaction technique

## Objectif

Vous aimez apprendre de nouvelles technologies ? Vous appréciez encore plus les expliquer à d'autres personnes ? La recherche du mot juste ne vous fait pas peur ? Votre profil nous intéresse !

L'objectif du stage de rédaction technique est de participer activement à la création et à la rédaction de nouveaux tutoriels et ce, pour au moins un des projets logiciels suivants : Splash, shmdata et LivePose.

Afin de faciliter l'utilisation de ses outils, le Metalab rédige, édite et maintient des sites Internet de documentation à leur sujet. Ces sites contiennent les informations essentielles à la découverte et à l'apprentissage des diverses fonctionnalités des outils du Metalab.

## Tâches

Sous supervision des membres de l'équipe du Metalab:

* Explorer les logiciels afin d'en comprendre les principaux types d'utilisation, et ce, à partir d'une utilisation se basant sur la documentation exitante;
* Identifier avec l'un des membres du Metalab des cas d'usage à documenter;
* Faire un plan de rédaction d'un tutoriel portant sur ces cas d'usage;
* Rédiger les tutoriels;
* Améliorer les tutoriels suite aux suggestions de l'équipe.

## Environnement de travail

Les outils que nous utilisons pour la documentation sont Sphinx (avec les formats markdown ou rst) et GitLab.

Notre environnement de travail favorise le travail en équipe et l'utilisation de logiciels libres, notamment par le biais de:

* Scrums et révisions de code
* GitLab, Markdown
* Linux, outils libres
* LaTeX
