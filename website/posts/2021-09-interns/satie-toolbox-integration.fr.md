<!--
.. title: SATIE - intégrations
.. slug: satie-outils-integration
.. date: 2022-01-17
.. tags: stages
.. category: audio
.. link:
.. description:
.. type: filled
.. author: Michał Seta
.. intern: Victor Comby
.. mentors: Michał Seta
-->


# Titre de stage
SATIE - intégrations

# Objective

SATIE est notre spatialisateur audio interne qui se veut flexible, robuste et qui supporte de nombreux arrangements différents de réseaux de haut-parleurs.
Nous réfléchissons à la création d'une boîte à outils ou d'un intergiciel afin de le rendre plus accessible et utile aux musiciens, compositeurs et artistes sonores qui ne sont pas programmeurs.
L'objectif de ce stage est de nous aider à identifier les outils et les techniques possibles pour rendre SATIE plus facilement intégrable aux projets d'audio spatial typiques.
Le candidat idéal aura une expérience et une compréhension des pipelines numériques de musique interactive et sera capable et intéressé par les aspects techniques de l'intégration de pipelines logiciels.

# Tâches

- Participer à l'idéation
- Développer des prototypes de pipelines d'intégration impliquant des outils de production musicale et SATIE.
- Participer au développement d'une démo (vidéo ou live)
- Participer à la vie du laboratoire : scrums, revue de code, etc.
- Documenter le travail et assurer sa reproductibilité.

# Contexte et logiciels

- Principalement Linux OS
- Traitement du signal numérique
- SuperCollider, Bash, Python
- Audio 3D, contrôle du spectacle
- Blocs de construction possibles : Max4Live, OSSIAscore, Chataigne
