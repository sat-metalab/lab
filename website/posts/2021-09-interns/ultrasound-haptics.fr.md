<!--
.. title: Développement d'un kit ouvert pour créer des effets haptiques avec des affichages ultrasonores
.. slug: ultrasound-haptics
.. date: 2022-10-11
.. tags: stages, haptics, ultrasound
.. category: haptics
.. link:
.. description:
.. type: filled
.. intern: Ezra Pierce
.. mentors: Christian Frisson, Edu Meneses, Michał Seta
.. author: Christian Frisson
-->

# Titre du stage

Développement d'un kit ouvert pour créer des effets haptiques avec des affichages ultrasonores

# Contexte

Formé depuis 2002, le Metalab est le laboratoire de recherche de la Société des Arts Technologiques [[SAT](https://sat.qc.ca/fr/recherche/metalab)]. 
La mission du [Metalab](https://sat-metalab.gitlab.io) est double: 

1. stimuler l'émergence d'expériences immersives innovantes, 
2. rendre leur conception accessible aux artistes et aux créateurs de l'immersion à travers un écosystème de logiciels libres.

En plus de notre ensemble d'outils pour le rendu audio et visuel, nous explorons des pistes pour augmenter les expériences immersives avec des retours haptiques, pour créer des simulations qui stimulent notre sens du toucher. Un premier dispositif de rendu haptique créé au Metalab est le [Plancher Haptique](https://sat.qc.ca/fr/nouvelles/sat-scalable-haptic-floor) qui transmet un retour haptique à nos corps à travers leurs points de contact avec le sol. Complémentairement, l'haptique ultrasonore produit un rendu haptique transmis dans l'air, sans contact, et sans nécessité de porter des dispositifs [[1](#1),[2](#2)], ouvrant de nouvelles perspectives pour l'immersion à travers un rendu haptique focalisé.

# Objectif

L'objectif principal de ce stage est de développer un kit ouvert pour créer des effets haptiques avec des affichages ultrasonores, réutilisant des travaux de recherche récents [[1](#1),[2](#2)], et facilitant la création et le développement d'effets haptiques ultrasonores pour les artistes et créateurs de l'immersion. 

Ce kit pourrait être complémenté par d'autres outils du [Metalab](https://sat-metalab.gitlab.io), comme [Poire](https://gitlab.com/sat-metalab/poire) pour le rendu haptique à l'aide de signaux audio, similairement au [Plancher Haptique](https://sat.qc.ca/fr/nouvelles/sat-scalable-haptic-floor), et [LivePose](https://sat-metalab.gitlab.io/documentations/livepose/) pour la détection de postures corporelles et manuelles pour élaborer des scénarios interactifs.

# Tâches

- Passer en revue les effets haptiques qui peuvent être générés par des affichages ultrasonores.
- Développer le kit ouvert (matériel et logiciel, réutilisant des travaux de recherche récents comme [[1](#1),[2](#2)]).
- Créer une vidéo de démo.
- Participer à la vie du laboratoire: scrums, revues de code, etc.
- Documenter notre travail dans un cahier de laboratoire numérique et dans des dépôts git et assurer sa reproductibilité.
- Co-écrire une publication dans le contexte de ce stage.

# Environnement de travail

- Déploiement matériel: Arduino ou Raspberry Pi
- Développement logiciel: C++, JavaScript ou Python
- Documentation: BibTeX, LaTeX, Markdown
- Processus: Confluence, Gitlab, Jira
- Ecosystème: Linux, Logiciels Libres (FOSS)

# Références

- <a name="1">[1]<a> Asier Marzo, Tom Corkett, and Bruce W. Drinkwater, “Ultraino: An Open Phased-Array System for Narrowband Airborne Ultrasound Transmission,” <i>IEEE Transactions on Ultrasonics, Ferroelectrics, and Frequency Control</i> 65, no. 1 (January 2018): 102–11, <a href="https://doi.org/10.1109/TUFFC.2017.2769399">DOI: 10.1109/TUFFC.2017.2769399</a>. 
Compléments: 
[github](https://github.com/asiermarzo/Ultraino/) 
[instructables](https://www.instructables.com/Ultrasonic-Array/) 
[makerfabs](https://www.makerfabs.com/index.php?route=product/product&product_id=508)  
- <a name="2">[2]<a> Rafael Morales et al., “Generating Airborne Ultrasonic Amplitude Patterns Using an Open Hardware Phased Array,” <i>Applied Sciences</i> 11, no. 7 (January 2021): 2981, <a href="https://doi.org/10.3390/app11072981">DOI: 10.3390/app11072981</a>. 
Compléments: 
[github](https://github.com/upnalab/SonicSurface)
