<!--
.. title: SATIE - toolbox integrations
.. slug: satie-toolbox-integration
.. date: 2022-01-17
.. tags: internships
.. category: audio
.. link:
.. description:
.. type: filled
.. author: Michał Seta
.. intern: Victor Comby
.. mentors: Michał Seta
-->


# Internship title
SATIE - toolbox integrations

# Objective

SATIE is our in-house audio spatializer that aims to be flexible, robust and supports many different arrangements of speaker arrays.
We are pondering a toolbox/middleware in an effort to make it more approachable and useful to musicians, composers and sound artists who are not programmers.
The objective of this internship is to help us to identify possible tools and techniques to make SATIE more easily integrable to typical spatial audio projects.
The ideal candidate will have experience and understanding with interactive music digital pipelines and be able and interested to tackle technical aspects of integrating software pipelines.

# Tasks

- Participate in the ideation
- Develop prototypes of integration pipelines involving music production tools and SATIE
- Participate in the development of a demo (video or live)
- Participate in the life of the laboratory: scrums, code review, etc.
- Document the work and ensure its reproducibility.

# Context and software

- Mostly Linux OS
- Digital signal processing
- SuperCollider, Bash, Python
- 3D audio, show control
- Possible building blocks: Max4Live, OSSIAscore, Chataigne,
