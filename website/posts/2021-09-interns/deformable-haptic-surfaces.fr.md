<!--
.. title: Développement d'un kit ouvert pour créer des surfaces haptiques déformables
.. slug: deformable-haptic-surfaces
.. date: 2022-01-10
.. tags: stages, haptics, déformable, surfaces, Splash
.. category: haptics
.. link:
.. description:
.. type: filled
.. author: Christian Frisson
.. intern: Raphaël Suzor-Schröder
.. mentors: Christian Frisson, Nicolas Bouillot
-->

# Titre du stage

Développement d'un kit ouvert pour créer des surfaces haptiques déformables

# Contexte

Formé depuis 2002, le Metalab est le laboratoire de recherche de la Société des Arts Technologiques [[SAT](https://sat.qc.ca/fr/recherche/metalab)]. 
La mission du [Metalab](https://sat-metalab.gitlab.io) est double: 

1. stimuler l'émergence d'expériences immersives innovantes, 
2. rendre leur conception accessible aux artistes et aux créateurs de l'immersion à travers un écosystème de logiciels libres.

En plus de notre ensemble d'outils pour le rendu audio et visuel, nous explorons des pistes pour augmenter les expériences immersives avec des retours haptiques, pour créer des simulations qui stimulent notre sens du toucher. Un premier dispositif de rendu haptique créé au Metalab est le [Plancher Haptique](https://sat.qc.ca/fr/nouvelles/sat-scalable-haptic-floor) qui transmet un retour haptique à nos corps à travers leurs points de contact avec le sol. Complémentairement, les surfaces haptiques déformables produisent un rendu haptique à travers des surfaces en périphérie, au dessus du sol [[1](#1)], ouvrant de nouvelles perspectives pour l'immersion à travers un rendu haptique interactif au-delà de configurations de bureau [[2](#2)].

# Objectif

L'objectif principal de ce stage est de développer un kit ouvert pour créer des surfaces haptiques déformables, réutilisant des travaux de recherche récents [[1](#1)], et facilitant l'utilisation de surfaces haptiques déformables pour les artistes et créateurs de l'immersion. 

Ce kit pourrait complémenter d'autres outils du [Metalab](https://sat-metalab.gitlab.io), comme informer [Splash](https://sat-metalab.gitlab.io/documentations/splash/) des déformations de surface pour la projection vidéo sur ces surfaces déformables.

# Tâches

- Passer en revue les méthodes d'actuation relatives. 
- Développer le kit ouvert (matériel et logiciel, réutilisant des travaux de recherche récents comme [[1](#1)).
- Créer une vidéo de démo.
- Participer à la vie du laboratoire: scrums, revues de code, etc.
- Documenter notre travail dans un cahier de laboratoire numérique et dans des dépôts git et assurer sa reproductibilité.
- Co-écrire une publication dans le contexte de ce stage.

# Environnement de travail

- Déploiement matériel: Arduino ou Raspberry Pi
- Développement logiciel: C++, JavaScript ou Python
- Documentation: BibTeX, LaTeX, Markdown
- Processus: Confluence, Gitlab, Jira
- Ecosystème: Linux, Logiciels Libres (FOSS)

# Références

- <a name="1">[1]<a> Anthony Steed et al., “A Mechatronic Shape Display Based on Auxetic Materials,” <i>Nature Communications</i> 12, no. 1 (August 6, 2021): 4758, <a href="https://doi.org/10.1038/s41467-021-24974-0">DOI: 10.1038/s41467-021-24974-0</a>.
- <a name="2">[2]<a> Y. Jansen and P. Dragicevic, “An Interaction Model for Visualizations Beyond The Desktop,” <i>IEEE Transactions on Visualization and Computer Graphics</i> 19, no. 12 (December 2013): 2396–2405, <a href="https://doi.org/10.1109/TVCG.2013.134">DOI: 10.1109/TVCG.2013.134</a>. 
[HAL](https://hal.inria.fr/hal-00847218v2)