<!--
.. title: Développer une interface orientée utilisateur pour Splash, notre logiciel de cartographie vidéo.
.. slug: user-oriented-splash-ui
.. author: Emmanuel Durand
.. date: 2022-09-18 10:19:11 UTC-05:00
.. tags: stages, imgui, Splash, gui
.. link:
.. description:
.. type: text
.. mentors: Emmanuel Durand
-->

# Intership title

Développement d'une interface orientée utilisateur pour Splash, notre logiciel de cartographie vidéo.

# Objective

Ce stage vise à faciliter l'utilisation de [Splash](https://sat-mtl.gitlab.io/documentation/splash), notre logiciel libre de *video-mapping*. L'interface utilisateur actuelle est orientée vers les développeurs et, en tant que telle, est très verbeuse et n'automatise pas une grande partie du processus de mappage vidéo (à l'exception de parties comme le calibrage des projecteurs et le mélange).

L'objectif serait, en utilisant la même technologie que celle déjà en place (dont [Dear ImGui] (https://github.com/ocornut/imgui/) qui est devenu un standard pour les interfaces utilisateur dans l'industrie du jeu vidéo), de transformer l'interface actuelle pour lui ajouter un nouveau mode simplifié qui cacherait la plupart de la complexité derrière Splash. Avec le soutien de l'équipe du Metalab, cette interface sera également utilisée pour contrôler Splash à travers une page web.

# Tasks

- Participer à l'idéation
- Participez à la maintenance du logiciel, en particulier du code lié à l'interface utilisateur.
- Contribuer au code source d'un logiciel libre
- Participer à l'implémentation d'une nouvelle interface utilisateur.
- Documenter le travail et en faire des démos/tutoriels.

# Context and software

- Équipe multidisciplinaire
- Réunions hebdomadaires
- C++/Python/OpenGL/Linux
- Dear ImGui/Emscripten
- Gitlab/Git
