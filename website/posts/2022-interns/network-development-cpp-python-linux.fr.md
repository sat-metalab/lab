<!--
.. title: Développement C++/Python/Linux pour transmission audiovisuelle en réseau
.. slug: dev-libre-av-reseau
.. date: 2022-10-11
.. tags: stages
.. category: dev
.. link:
.. description:
.. type: closed
.. author: Nicolas Bouillot
.. intern: 
.. mentors: Nicolas Bouillot
-->

# Titre du stage
Développement C++/Python/Linux pour transmission audiovisuelle en réseau

# Objectif
Ce stage vise à contribuer au développement et la mise en œuvre du logiciel Switcher, un logiciel permettant la transmission audiovisuelle multicanal en basse latence. Le logiciel est implémenté en C++, avec un wrapper Python. Différents protocoles de transmission y sont implémentés : SIP/RTP, RTMP, OSC et NDI.   

[Code de Switcher](https://gitlab.com/sat-mtl/tools/switcher)

# Tâches

- Participer à la maintenance du logiciel à travers le processus de contribution ouvert
- Participation au suivi du backlog sous gitlab
- Contribuer au code source du logiciel libre
- Participer à la mise en œuvre de Switcher dans un projet de transmission multicanal avec des partenaires du Metalab 
- Participer à l'intégration de Switcher dans une architecture d'outils, en vue de déploiement de service dans le Cloud

# Contexte et logiciels

- Équipe pluridisciplinaire
- Rencontres hebdomadaires
- C++/Python/CMake/Linux
- Protocoles réseau
- Ligne de commande et script
- Outils mesure et surveillance de réseau
