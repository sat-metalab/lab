<!--
.. title: Intégration web - sites Internet du Metalab
.. slug: integration-web
.. date: 2022-10-11
.. tags: stages, internships
.. category: web
.. link:
.. description:
.. type: 
.. author: Metalab
.. intern: 
.. mentors: Edith Viau et Thomas Piquet
-->

# Titre du stage

Intégration web - sites Internet du Metalab

## Objectif

L'objectif de ce stage en intégration web est de faciliter l'entretien et la mise-à-jour des différents sites web du Metalab en créant une structure unifiée de développement.

Le Metalab, le département de recherche et de développement de la [Société des arts technologiques](https://www.sat.qc.ca) maintient différents sites Internet afin de communiquer l'état de ses travaux au grand public. Ces sites webs sont présentement hébergés sur Gitlab dans plusieurs dépôts. Nous aimerions avoir un seul dépôt et un unique cadriciel de génération de site webs statiques.

## Tâches

- Prendre connaissance des différents sites webs concernés
- Établir une approche de migration des sites webs vers un dépôt Gitlab unique
- Confirmer cette approche avec l'équipe du Metalab
- Réaliser la migration

## Contexte et logiciels

- Sphinx: générateur de sites webs pour la documentation
- Git et Gitlab
- HTML/CSS/JavaScript
