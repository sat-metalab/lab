<!--
.. title: Development of a plugin for a bibliography management tool to facilitate exploring trends
.. slug: zotero-infovis-trends-plugin
.. date: 2022-10-11
.. tags: internships
.. category: 
.. link:
.. description:
.. type: filled
.. author: Christian Frisson
.. intern: Yassin Akbib
.. mentors: Christian Frisson
-->

# Title of the internship

Development of a plugin for a bibliography management tool to facilitate exploring trends

# Objective

At Metalab and other departments for innovation at the SAT, as in many Research and Development groups, we regularly perform technological watch to keep up to date with recent advances in technologies, in our case enabling the creation of immersive multimedia experiences. We use [Zotero](https://www.zotero.org) to share collections of bibliographical items. 

A few Zotero plugins have been created to perform trend exploration tasks on collections using information visualization and text mining, but they are either deprecated ([Paper Machines](https://www.papermachines.org), [zotero-voyant-export](https://github.com/corajr/zotero-voyant-export) for [Voyant Tools](https://voyant-tools.org)), or [in progress](https://github.com/search?q=zotero+visualization&ref=opensearch). 

The goal of this internship is to develop a plugin for Zotero to facilitate exploring trends, for now in collections of PDF files. Restoring deprecated plugins or contributing to in-progress plugins or starting from scratch are viable options.

# Tasks

- Review existing Zotero plugins
- Understand key techniques from information visualization and text mining to support tasks such as exploring trends
- Develop a plugin (restore deprecated plugins or contribute to in-progress plugins or start from scratch)
* (Bonus) Present our joint contributions at an international conference

# Context and software

- [Zotero](https://www.zotero.org) [Plugin Development](https://www.zotero.org/support/dev/client_coding/plugin_development): JavaScript, CSS, Mozilla technologies (XPCOM, XULRunner, XUL, etc.), Git
- Related [Zotero plugins](https://www.zotero.org/support/plugins): [Paper Machines](https://www.papermachines.org), [zotero-voyant-export](https://github.com/corajr/zotero-voyant-export) for [Voyant Tools](https://voyant-tools.org), and [more](https://github.com/search?q=zotero+visualization&ref=opensearch)...
