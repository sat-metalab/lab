<!--
.. title: Live crowd animation
.. slug: live-crowd-animation
.. date: 2022-10-11
.. tags: internships
.. category: 3d
.. link:
.. description:
.. type: filled
.. author: Nicolas Bouillot
.. intern: Manuel Bolduc
.. mentors: Christian Frisson, Emmanuel Durand
-->

# Title of the internship
Live crowd animation

# Objective
In the context of live virtualization of a symphonic concert, a video capture of the artists and the audience allows to extract movement information from the crowd. The objective of this internship is to prototype a simple approach to model in real time the activity of the concert hall in an online 3d environment.


# Tasks

- Participate in the ideation of the approach.
- Implement the approach, in collaboration with the research and development teams.
- Participate in a symphony orchestra capture
- 3d avatar modeling for performers and public 
- Develop animation strategies
- Participate and accompany the integration of the animation system in an online 3d environment


# Context and software

- [LivePose](https://gitlab.com/sat-mtl/tools/livepose)
- Data flow mapping
- Mozilla Hubs through the SAT instance, called [Satellite](https://sat.qc.ca/fr/satellite#section)
- Blender
