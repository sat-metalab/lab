<!--
.. title: Web integration - Metalab websites
.. slug: integration-web
.. date: 2022-10-11
.. tags: stages, internships
.. category: web
.. link:
.. description:
.. type: 
.. author: Metalab
.. intern: 
.. mentors: Edith Viau and Thomas Piquet
-->

# Internship title

Web integration - Metalab websites

## Goal

The goal of this internship in web integration is to improve the maintenance and the update process of the Metalab websites by creating a unified development structure.

The Metalab is the R&D lab of the [Society for Arts and Technology](https://www.sat.qc.ca). We currently maintain a collection of websites, hosted on Gitlab in many different repositories. These websites are used to communicate with the general public about the state of our research work. We would like to have a single and unique framework for static site generation.

## Tasks

- Take account of the Metalab websites
- Design a migration strategy to move the websites into a single Gitlab repo
- Present for approval this strategy to the Metalab team
- Implement the migration

## Context and software

- Sphinx: static site generator for documentation
- Git and Gitlab
- HTML/CSS/JavaScript
