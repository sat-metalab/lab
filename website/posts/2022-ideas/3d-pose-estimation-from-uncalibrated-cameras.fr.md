<!--
.. title: Estimation de pose en 3D à partir de caméras multiples non-calibrées
.. slug: idea-3d-pose-from-multiple-cameras
.. author: Emmanuel Durand
.. date: 2022-10-11
.. tags: internships, ideas, hard, 350 hours, livepose, stages
.. type: text
-->

<!-- Please follow and keep (do not delete) all of these comments! -->

<!--
This template has been created using the following resources:
https://google.github.io/gsocguides/mentor/defining-a-project-ideas-list
https://google.github.io/gsocguides/mentor/making-your-ideas-page
-->

<!-- 
Please update post metadata: 
* title
* slug (unique string used in URL, dash-separated list of lowercase words, starting with idea-, identical as filename)
* author (comma-separated list of mentors)
* date (not in future otherwise not visible now when website is built)
* tags (comma-separated, starting with: ideas, including difficulty: easy or medium or hard; size: 175 hours or 350 hours; tools from SAT) 
-->

## Titre du projet et description

<!-- Please add title below this comment to match the value in post title metadata -->

Estimation de pose en 3D à partir de caméras multiples non-calibrées

## Description détaillée du projet

<!-- Please write 2-5 sentences below this comment --> 

[LivePose](https://sat-metalab.gitlab.io/livepose) est un logiciel libre de détection de pose et d'action pouvant utiliser plusieurs caméras dans le but d'aider à la création d'expériences interactives.

Avec l'utilisation de caméras multiples arrive le besoin de les calibrer ensemble, de façon à pouvoir extraire des coordonnées 3D à partir des estimations 2D de pose associées à chaque caméra. ce projet vise à faciliter le processus de calibration par l'utilisation du corps humain en tant qu'outil de calibration, enlevant par le fait même le recours à des points de calibrage ou à d'autres méthodes encombrantes.

Quelques liens pertinents tirés de la littérature sur le calibrage basé sur le corps humain :

* [Human Pose as Calibration Pattern; 3D Human Pose Estimation with Multiple Unsynchronized and Uncalibrated Cameras](https://openaccess.thecvf.com/content_cvpr_2018_workshops/papers/w34/Takahashi_Human_Pose_As_CVPR_2018_paper.pdf)
* [MetaPose: Fast 3D Pose from Multiple Views without 3D Supervision](https://arxiv.org/abs/2108.04869) (celui-ci fait usage de l'apprentissage profond afin d'obtenir de meilleurs résultats, quitte à engendrer un coût de calcul plus élevé)

## Résultats attendus

<!-- Please add 2-5 items below this comment --> 

* Implémentation d'un sous-module à partir de l'un des articles cités ci-haut, aux côtés d'un prototype ou d'un travail similaire (à déterminer au début du projet)
* Représentation partielle des résultats par l'entraînement puis le passage au test sur un sous-ensemble d'un jeu de données disponible publiquement

## Compétences

<!-- Please add 2-5 items below this comment --> 

* (requis) expérience avec Python
* (requis) expérience avec la vision par ordinateur
* (requis) connaissances fondamentales en mathématiques
* dans le cas où de l'apprentissage profond fait partie du projet: expérience avec des cadriciels d'apprentissage machine

## Possibles mentors

<!-- Please list yourself/yourselves. 2 possible mentors are more failsafe. --> 

[Emmanuel Durand](https://gitlab.com/paperManu), [Christian Frisson](https://gitlab.com/christianfrisson)


## Durée planifiée du projet

<!-- Please write below this comment either: 175 hours or 350 hours -->

350 heures

## Niveau de difficulté 

<!-- Please write below this comment either: easy, medium or hard -->

difficile
