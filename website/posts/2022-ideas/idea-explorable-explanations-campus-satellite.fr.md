<!--
.. title: Explications explorables pour l'enseignement de concepts en arts numériques dans un campus hybride en téléprésence
.. slug: idea-explorable-explanations-campus-satellite
.. author: Christian Frisson, Edith Viau
.. mentors: Christian Frisson
.. date: 2022-10-11
.. tags: ideas, hard, 350 hours, Satellite, internships, stages
.. type: text
-->

<!-- Please follow and keep (do not delete) all of these comments! -->

<!-- 
This template has been created using the following resources:
https://google.github.io/gsocguides/mentor/defining-a-project-ideas-list
https://google.github.io/gsocguides/mentor/making-your-ideas-page
-->

<!-- 
Please update post metadata: 
* title
* slug (unique string used in URL, dash-separated list of lowercase words, starting with idea-, identical as filename)
* author (comma-separated list of mentors)
* date (not in future otherwise not visible now when website is built)
* tags (comma-separated, starting with: ideas, including difficulty: easy or medium or hard; size: 175 hours or 350 hours; tools from SAT) 
-->

## Titre et description du projet

<!-- Please add title below this comment to match the value in post title metadata -->

Explications explorables pour l'enseignement de concepts en arts numériques dans un campus hybride en téléprésence

## Description détaillée du projet

<!-- Please write 2-5 sentences below this comment --> 

Des explications explorables permettent d'ajouter des éléments interactifs aux documents et aux graphes en plus de rendre possible l'association d'illustrations et de mots, ceci afin de mettre de l'avant différentes façons d'apprendre des concepts complexes tout en jouant avec leurs paramètres. Des exemples sont disponibles sur [https://explorabl.es](https://explorabl.es).

À partir de 2020 et en réponse à la crise sanitaire, le [Hub Satellite](https://hub.satellite.sat.qc.ca/about/), conçu à partir de [Mozilla Hubs](https://github.com/mozilla/hubs/), est un environnement web 3D pour la socialisation qui est développé à la SAT afin de promouvoir des contenus artistiques et culturels variés tout en créant une synergie en les interconnectant, ce qui inclut l'enseignement dans des environnements de type campus, en téléprésence.

Nous souhaitons obtenir votre aide quant à l'enseignement en mode hybride et en téléprésence par la réalisation de prototypes d'explications explorables qui pourront être par la suite diffusées dans Satellite / Mozilla Hubs. Ces prototypes peuvent inclure des fonctionnalités d'interactivité.

## Résultats attendus

<!-- Please add 2-5 items below this comment --> 

* Prototypage d'explication explorable en JavaScript
* Intégration des prototypes dans [Mozilla Hubs](https://github.com/mozilla/hubs/) utilisé pour [Satellite](https://gitlab.com/sat-mtl/satellite/hubs-injection-server) par l'adaptation de [Spoke](https://github.com/mozilla/Spoke) de façon à définir des contenus dynamiques et interactifs
* (Optionnel) Présentation de notre contribution commune à une conférence internationale

## Compétences

<!-- Please add 2-5 items below this comment --> 

* (pré-requis) expérience avec JavaScript
* (atout) compréhension des fondamentaux en technologies des arts numériques, incluant mais non-limitée aux graphiques par ordinateur, à la vision par ordinateur, à l'apprentissage machine, au traitement de signal sonore, à l'haptique

## Mentor possible

<!-- Please list yourself/yourselves. 2 possible mentors are more failsafe. -->

[Christian Frisson](https://gitlab.com/christianfrisson)

Les mentors pour cette idée de projet peuvent partager leur expérience avec des contributeur.trice.s potentiel.le.s au Google Summer of Code pour ce projet, dans le développement d'explications explorables pour des disciplines variées reliées aux arts numériques :

* traitement de signal sonore : [The IDMIL Digital Audio Workbench](https://idmil.github.io/DAWb/)
<!-- Marcelo Wanderley, Travis West, Josh Rohs, Eduardo Meneses, and Christian Frisson. 2021. [The IDMIL Digital Audio Workbench](https://idmil.github.io/DAWb/): An interactive online application for teaching digital audio concepts. 16th AudioMostly Conference on Interaction with Sound. ACM. DOI: [10.1145/3478384.3478397](https://doi.org/10.1145/3478384.3478397) -->
* haptique à l'aide de WebAudio (diapositives) : [WebAudioHaptics](https://WebAudioHaptics.github.io)
<!-- Christian Frisson, Thomas Pietrzak, Siyan Zhao, and Ali Israr. 2016. [WebAudioHaptics](https://WebAudioHaptics.github.io): Tutorial on Haptics with Web Audio. 2nd Web Audio Conference. WAC'16 -->
* visualisation de l'information avec [Idyll](https://github.com/idyll-lang/)
<!-- Jagoda Walny, Christian Frisson, Mieka West, Doris Kosminsky, Søren Knudsen, Sheelagh Carpendale, Wesley Willett. 2020. Data Changes Everything: Challenges and Opportunities in Data Visualization Design Handoff. IEEE Transactions on Visualization and Computer Graphics 26, 1. TVCG. DOI: [10.1109/TVCG.2019.2934538](https://doi.org/10.1109/TVCG.2019.2934538) -->

## Durée attendue du projet

<!-- Please write below this comment either: 175 hours or 350 hours -->

350 heures

## Degrée de difficulté

<!-- Please write below this comment either: easy, medium or hard -->

difficile
