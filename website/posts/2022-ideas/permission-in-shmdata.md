<!--
.. title: Add support of Unix permission in the Shmdata library
.. slug: idea-shmdata-permission
.. author: Nicolas Bouillot
.. date: 2022-02-17 07:24:11 UTC-05:00
.. tags: internships, ideas, medium, 175 hours
.. type: filled
.. author: Nicolas Bouillot
.. intern: Vanshita Verma
.. mentors: Nicolas Bouillot, Thomas Piquet, Valentin Laurent
-->

<!-- Please follow and keep (do not delete) all of these comments! -->

<!-- 
This template has been created using the following resources:
https://google.github.io/gsocguides/mentor/defining-a-project-ideas-list
https://google.github.io/gsocguides/mentor/making-your-ideas-page
-->

<!-- 
Please update post metadata: 
* title
* slug (unique string used in URL, dash-separated list of lowercase words, starting with idea-, identical as filename)
* author (comma-separated list of mentors)
* date (not in future otherwise not visible now when website is built)
* tags (comma-separated, starting with: ideas, including difficulty: easy or medium or hard; size: 175 hours or 350 hours; tools from SAT) 
-->

## Project title/description

<!-- Please add title below this comment to match the value in post title metadata -->
The [Shmdata](https://gitlab.com/sat-metalab/shmdata) library provide a layer to share streams of framed data between processes via shared memory. It supports any kind of data stream: it has been used with multichannel audio, video frames, 3D models, OSC messages, and various others types of data. Shmdata is server-less, but requires applications to link data streams using socket path (e.g. "/tmp/my-shmdata-stream"). Shmdata is very fast and allows processes to access data streams without the need for extra copies.

However, permissions are not supported, and any stream shared on the system with Shmdata is accessible system-wide. The project will enable the possibility for a Shmdata user to configure streams permission using shared memory right setting at the API level.  

## More detailed description of the project

<!-- Please write 2-5 sentences below this comment -->
The project consists in adding methods for support of Unix Shared memory in the C++ core of the library. C++ tests will be needed in order to ensure the core library handles well permission related issues. Possibly, the new feature will be made available in Shmdata python wrapper and Shmdata GStreamer elements.

## Expected outcomes

<!-- Please add 2-5 items below this comment --> 

* contribution of the new feature to the library
* update of the Shmdata documentation

## Skills required/preferred

<!-- Please add 2-5 items below this comment --> 

* C++
* SysV Shared Memory, Unix Socket programming
* interest/experience with audio/video/datastream programming
* interest/experience with Python C API
* interest/experience with GStreamer element writing

## Possible mentors

<!-- Please list yourself/yourselves. 2 possible mentors are more failsafe. --> 

[Nicolas Bouillot](https://gitlab.com/nicobou), [Valentin Laurent](https://gitlab.com/vlaurent)

## Expected size of project 

<!-- Please write below this comment either: 175 hours or 350 hours -->

175 hours

## Rating of difficulty 

<!-- Please write below this comment either: easy, medium or hard -->

medium
