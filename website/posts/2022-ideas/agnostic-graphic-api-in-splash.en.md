<!--
.. title: Replace OpenGL by a multi-API rendering library in Splash
.. slug: idea-splash-rendering-api
.. author: Emmanuel Durand
.. date: 2022-10-11
.. tags: ideas, hard, 350 hours, internships
.. type: text
-->

<!-- Please follow and keep (do not delete) all of these comments! -->

<!--
This template has been created using the following resources:
https://google.github.io/gsocguides/mentor/defining-a-project-ideas-list
https://google.github.io/gsocguides/mentor/making-your-ideas-page
-->

<!-- 
Please update post metadata: 
* title
* slug (unique string used in URL, dash-separated list of lowercase words, starting with idea-, identical as filename)
* author (comma-separated list of mentors)
* date (not in future otherwise not visible now when website is built)
* tags (comma-separated, starting with: ideas, including difficulty: easy or medium or hard; size: 175 hours or 350 hours; tools from SAT) 
-->

## Project title/description

<!-- Please add title below this comment to match the value in post title metadata -->

The [Splash](https://sat-metalab.gitlab.io/splash) projection mapping software allows for controlling multiple videoprojectors together to build a single projection. It is able to adapt to virtually any real geometry, as long as the surface is diffuse. Its rendering engine is built using the [OpenGL API](https://www.khronos.org/opengl/). It currently runs on platforms capable of runing Linux and handling an OpenGL 4.5 context, and has been tested successfully on x86_64 (with NVIDIA, AMD and Intel graphic cards) and aarch64 (with NVIDIA graphic cards)

However to be able to a) optimize it further and b) support more platforms (for example Raspberry Pi), it would be interesting to support more graphics API. To do this it is envisionned to replace direct use of OpenGL with an intermediate, multi-API rendering library. For now [bgfx](https://github.com/bkaradzic/bgfx) is considered.

## More detailed description of the project

<!-- Please write 2-5 sentences below this comment --> 

For now everything related to graphic rendering is done through direct calls to OpenGL, which means that OpenGL is quite intertwined with the source code of Splash. A first step could be to isolate a bit more the OpenGL calls, which would then facilitate replacing these calls with calls to the multi-API rendering library.

## Expected outcomes

<!-- Please add 2-5 items below this comment --> 

* cleanup of the Splash rendering code
* added support for at least the Vulkan API
* possibility to run Splash on new platforms

## Skills required/preferred

<!-- Please add 2-5 items below this comment --> 

* C++
* OpenGL, other graphics API would be a plus
* experience with game/rendering engine programming

## Possible mentors

<!-- Please list yourself/yourselves. 2 possible mentors are more failsafe. --> 

Emmanuel Durand

## Expected size of project 

<!-- Please write below this comment either: 175 hours or 350 hours -->

350 hours

## Rating of difficulty 

<!-- Please write below this comment either: easy, medium or hard -->

hard
