<!--
.. title: 3D pose estimation from multiple uncalibrated cameras
.. slug: idea-3d-pose-from-multiple-cameras
.. author: Emmanuel Durand
.. date: 2022-10-11
.. tags: internships, ideas, hard, 350 hours, livepose
.. type: text
-->

<!-- Please follow and keep (do not delete) all of these comments! -->

<!--
This template has been created using the following resources:
https://google.github.io/gsocguides/mentor/defining-a-project-ideas-list
https://google.github.io/gsocguides/mentor/making-your-ideas-page
-->

<!-- 
Please update post metadata: 
* title
* slug (unique string used in URL, dash-separated list of lowercase words, starting with idea-, identical as filename)
* author (comma-separated list of mentors)
* date (not in future otherwise not visible now when website is built)
* tags (comma-separated, starting with: ideas, including difficulty: easy or medium or hard; size: 175 hours or 350 hours; tools from SAT) 
-->

## Project title/description

<!-- Please add title below this comment to match the value in post title metadata -->

3D pose estimation from multiple uncalibrated cameras

## More detailed description of the project

<!-- Please write 2-5 sentences below this comment --> 

[LivePose](https://sat-metalab.gitlab.io/livepose) is an open-source pose and action detection software, capable of using multiple cameras to help with creating interactive experiences.

With using multiple cameras comes the need to calibrate them together, to extract 3D coordinates from the 2D pose estimations associated with each camera. This project aims for facilitating the calibration process by using the human body as a calibration tool, hence removing the need to manually place calibration points or any such cumbersome method.

Some preliminary literature describing a human-based calibration:

* [Human Pose as Calibration Pattern; 3D Human Pose Estimation with Multiple Unsynchronized and Uncalibrated Cameras](https://openaccess.thecvf.com/content_cvpr_2018_workshops/papers/w34/Takahashi_Human_Pose_As_CVPR_2018_paper.pdf)
* [MetaPose: Fast 3D Pose from Multiple Views without 3D Supervision](https://arxiv.org/abs/2108.04869) (this one makes use of deep learning to achieve better results at the cost of computation)

## Expected outcomes

<!-- Please add 2-5 items below this comment --> 

* Implementation of a sub-module from one of the papers above along with a working prototype (or of similar work found by the beginning of this project)
* Partial representation of results by training/testing on a subset of a publicly available dataset

## Skills required/preferred

<!-- Please add 2-5 items below this comment --> 

* required: experience with Python
* required: experience with computer vision
* required: strong foundation in math
* if going the deep learning road: experience with machine learning frameworks

## Possible mentors

<!-- Please list yourself/yourselves. 2 possible mentors are more failsafe. --> 

[Emmanuel Durand](https://gitlab.com/paperManu), [Christian Frisson](https://gitlab.com/christianfrisson)


## Expected size of project 

<!-- Please write below this comment either: 175 hours or 350 hours -->

350 hours

## Rating of difficulty 

<!-- Please write below this comment either: easy, medium or hard -->

hard
