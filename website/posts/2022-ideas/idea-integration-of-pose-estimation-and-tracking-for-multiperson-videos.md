<!--
.. title: Integration of pose estimation and tracking for multiperson videos
.. slug: idea-integration-of-pose-estimation-and-tracking
.. author: Farzaneh Askari
.. date: 2022-10-11
.. tags: ideas, hard, 350 hours, livepose, internships
.. type: filled
.. intern: Victor Rios
-->

<!-- Please follow and keep (do not delete) all of these comments! -->

<!-- 
This template has been created using the following resources:
https://google.github.io/gsocguides/mentor/defining-a-project-ideas-list
https://google.github.io/gsocguides/mentor/making-your-ideas-page
-->

## Project title/description

<!-- Please add title below this comment to match the value in post title metadata -->

Integration of pose estimation and tracking for multiperson videos

## More detailed description of the project

<!-- Please write 2-5 sentences below this comment --> 
 [LivePose](https://gitlab.com/sat-metalab/livepose/) is an open-source human skeleton and face keypoints detection software. There are many state-of-the-art pose estimation approaches integrated into Livepose. 

Often during immersive experiences, the participant moves around the scenes, resulting in frequent occlusions in given timestamps, while fully visible in the neighboring frames. This project aims to combine pose estimation and tracking for improved pose estimation of immersive media.

Our proposed method is inspired by the below studies in this field:

* [Self-supervised Keypoint Correspondences for Multi-Person Pose Estimation and Tracking in Videos](https://arxiv.org/pdf/2004.12652.pdf)
* [Combining detection and tracking for human pose estimation in videos](https://arxiv.org/pdf/2003.13743.pdf)

 
## Expected outcomes

<!-- Please add 2-5 items below this comment --> 

* Complete implementation of one of the papers above (or of similar work appearing or found by the beginning of this project)
* Partial representation of results by training and testing on one of the publicly available pose datasets 
* (Bonus) Adaptation components for a custom dataset (e.g., custom dataloader, etc)

## Skills required/preferred

<!-- Please add 2-5 items below this comment --> 

* required: experience with Python
* required: experience with machine learning frameworks (Pytorch preferred)
* required: understanding of the fundamentals of deep learning
* preferred: understanding of the fundamentals of computer vision

## Possible mentors

<!-- Please list yourself/yourselves. 2 possible mentors are more failsafe. -->

[Farzaneh Askari](https://gitlab.com/faskari), [Emmanuel Durand](https://gitlab.com/paperManu), [Christian Frisson](https://gitlab.com/christianfrisson)

## Expected size of project 

<!-- Please write below this comment either: 175 hours or 350 hours -->

350 hours

## Rating of difficulty 

<!-- Please write below this comment either: easy, medium or hard -->

hard
