<!--
.. title: Add support of SRT in Switcher, a multichannel low latency streamer
.. slug: idea-srt-in-switcher
.. author: Nicolas Bouillot
.. date: 2022-10-11
.. tags: ideas, hard, 350 hours, internships
.. type: closed
-->

<!-- Please follow and keep (do not delete) all of these comments! -->

<!-- 
This template has been created using the following resources:
https://google.github.io/gsocguides/mentor/defining-a-project-ideas-list
https://google.github.io/gsocguides/mentor/making-your-ideas-page
-->

<!-- 
Please update post metadata: 
* title
* slug (unique string used in URL, dash-separated list of lowercase words, starting with idea-, identical as filename)
* author (comma-separated list of mentors)
* date (not in future otherwise not visible now when website is built)
* tags (comma-separated, starting with: ideas, including difficulty: easy or medium or hard; size: 175 hours or 350 hours; tools from SAT) 
-->

## Project title/description

<!-- Please add title below this comment to match the value in post title metadata -->

[Switcher](https://gitlab.com/sat-metalab/switcher) is an integration environment, able to interoperate with other software and protocols. Switcher provides low latency streaming of multichannel audio, video and data through IP networks.

Switcher provides managing of several instances of services (called Quiddities). A Quiddity can be created and removed dynamically and can be controlled through property get/set as well as method invocations and information tree monitoring. Switcher provides introspection mechanisms to help write higher level software written in python3 or C++. Switcher can save and load the state of quiddities.

The project consists in writing a new Switcher Quiddity for the support of the [SRT protocol](https://www.matrox.com/en/video/media/guides-articles/srt-protocol). This protocol is regarded as of interest for video communication in the context of artistic telepresence, but also for potential streaming to diffusion platforms, as a replacement to the RTMP protocol. This project will have a positive impact on the [Scenes ouvertes (fr)](https://sat.qc.ca/fr/scenic-telepresence#section) telepresence network of artistic venues in Quebec, Canada.

## More detailed description of the project

<!-- Please write 2-5 sentences below this comment --> 

The projet will consist in writing a Quiddity that handles SRT, with audio and video streams as input. The GStreamer framework is already a strong dependency in Switcher, and already includes an integration of the protocol. The new Quiddity could therefore use the GStreamer implemention for integration. Accordingly, this project does not require strong knowledge of the SRT protocol. The main part of the project will involve C++ interfacing of GStreamer implementation into the Quiddity logic, and writing C++ or python test.



## Expected outcomes

<!-- Please add 2-5 items below this comment --> 

* Add new module (a Quiddity) for SRT streaming in Switcher
* Add C++ or python test for this Quiddity
* Demonstration of an audio/video streaming with the new Quiddity 

## Skills required/preferred

<!-- Please add 2-5 items below this comment --> 

* C++
* Ubuntu Linux
* interest/experience with GStreamer application programming
* possibly basic Python3

## Possible mentors

<!-- Please list yourself/yourselves. 2 possible mentors are more failsafe. --> 

[Nicolas Bouillot](https://gitlab.com/nicobou), [Valentin Laurent](https://gitlab.com/vlaurent)

## Expected size of project 

<!-- Please write below this comment either: 175 hours or 350 hours -->

350 hours

## Rating of difficulty 

<!-- Please write below this comment either: easy, medium or hard -->

hard
