.. title: Programmation Audio Spatial pour les Arts Technologiques
.. slug: prog-audio-cpp-python
.. date: 2021-10-12 17:00:00 UTC-04:00
.. tags: emplois 
.. category:
.. link: 
.. description: 
.. type: closed

Poste
-----

La Société des arts technologiques [SAT] a besoin de toi pour une entrée en fonction immédiate au sein du Metalab, le laboratoire de recherche de la SAT. Ton rôle sera de participer au processus de recherche : veille, idéation, développement, mise en œuvre, communication et participation aux projets en partenariat. Tu participeras à l'évolution, à la revue de code et l'utilisation des logiciels du Metalab, en particulier de nos outils de spatialisation audio (SATIE) et de simulation acoustique (vaRays). Une expérience d'au moins 5 ans est souhaitée.


Environnement technique
-----------------------

* Ubuntu, Linux, FOSS
* SuperCollider, Numpy
* C++, Python3
* Gitlab, JIRA, Confluence, Slack

Atouts
------
* Spatialisation, ambisonie
* Acoustique
* Blender
* GDB, cmake, clang

Environnement de travail
------------------------

Formé depuis 2002, le `Metalab <https://sat.qc.ca/fr/recherche/metalab>`__ est le laboratoire de recherche et développement de la Société des arts technologiques [`SAT <https://sat.qc.ca/>`_]. La mission du Metalab est double: stimuler l'émergence de d'expériences immersives innovantes et rendre leur conception accessible aux artistes et aux créateurs de l'immersion à travers un écosystème de logiciels libres.

Les thématiques de recherche du Metalab -la téléprésence, l'immersion, le mapping vidéo et le son spatialisé- sont développées à travers des projets de recherche et de production. L'équipe du Metalab compte entre 8 et 15 personnes et regroupe des expertises techniques variées : audionumérique, informatique graphique, réseaux, développement logiciel et intégration multimédia. Cette équipe accueille des stagiaires, y compris dans d'autres domaines de compétence.

L'écosystème de logiciels libres du Metalab a pour ambition de couvrir l'intégralité de la production de contenu immersif et distribué: l'édition d'environnement immersif, le video mapping, la spatialisation du son, l'interaction de groupe et la transmission basse latence. Ils sont développés pour être évolutifs sur le long terme: ils sont le support des recherches et des partenariats. En interne à la SAT, ces logiciels sont intégrés à la palette d'outils des résidences de création de la [SAT] et sont au cœur des projets de téléprésence, entre bibliothèques (`Bibliolab <https://sat.qc.ca/fr/bibliolab>`_) et entre salles de spectacle (`Scènes ouvertes <https://sat.qc.ca/fr/scenes-ouvertes>`_). Nos partenaires comptent notamment des artistes, des chercheurs universitaires, des écoles, des designers et des industriels.

Logiciels du Metalab
--------------------

* Édition et prototypage d'espace immersifs : `édition in situ (EiS) <https://gitlab.com/sat-metalab/EditionInSitu>`_
* Video mapping multi-projecteurs pour tous types de surface : `Splash <https://gitlab.com/sat-metalab/splash/-/wikis/home>`_
* Calibrage de projecteurs pour espaces immersif : `Calimiro <https://gitlab.com/sat-metalab/calimiro>`_
* Spatialisation du son pour tous types de système de haut-parleurs : `SATIE <https://gitlab.com/sat-metalab/SATIE>`_
* Spatialisation du son par simulation acoustique : `vaRays <https://gitlab.com/sat-metalab/varays>`_
* Interaction de groupe de personnes en environnement immersif : `LivePose <https://gitlab.com/sat-metalab/livepose>`_
* Téléprésence et transmission multicannal en basse latence : `Switcher <https://gitlab.com/sat-metalab/switcher>`_
* Partage de tous types de flux de données entre applications : `Shmdata <https://gitlab.com/sat-metalab/shmdata>`_
