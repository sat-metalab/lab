.. title: Strategies for diverse, open and inclusive open source software communities
.. slug: foss-communities
.. date: 2021-03-22
.. tags: internships
.. category:
.. link: 
.. description: 
.. type: closed

Internship title
----------------

Strategies for an open, diverse and inclusive software community - the case of Metalab open source softwares


Goal
----

As part of the Metalab's ongoing research projects on immersion and teleprescence, our team creates, develops and maintains a suite of free and open source software tools.
We promote the use of this toolbox for a variety of projects.

We wish to enable the creation and the development of an open, diverse, inclusive community surrounding this toolbox and its use, supporting community contributions to our software.

The goals of this internship are two-fold: first, to assess available strategies to help ensure an open and inclusive communtiy setting. 
The second step will be an analysis of their adequacy for the Metalab software projects.

These strategies will be part of a research report.

Tasks
-----

With support from the Metalab team, exploration of the following questions:

* How can we enable the emergence of an open, diverse and inclusive software community?
* Which tangible actions could be taken by the Metalab to foster community participation?

With support from the Metalab team, conduct a research project including:

* Exploration and documentation of community practices followed in arts communities
* Exploration and documentation of community practices followed in open source software communities
* Recommandations for community facilitation tools
* Recommandations for the Metalab to adopt such tools and practices

Work environment
----------------

Our environment promotes team work and the adoption of open source software, including:

* Scrums and code review
* GitLab, Python, Markdown, Bash
* Linux, Free and Open Source Software (FOSS)
* LaTeX

