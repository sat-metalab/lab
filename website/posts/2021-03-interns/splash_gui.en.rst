.. title: Development of a graphic interface for a video mapping software
.. slug: splash-gui
.. date: 2021-03-22
.. tags: internships
.. category:
.. link:
.. description:
.. type: closed


Internship title
----------------
Development of a graphic interface for a video mapping software

Objective
---------
The graphical interface is often the first layer between the user and software and determines whether the software is easily usable or not. This internship focuses on this essential component with the overall goal to revisit the current interface of Splash, our video mapping software, and propose a new graphical interface targeting both beginner and intermediate users.

Splash is used in a high performance context for the [SAT] productions in the Satosphere. In this context, it handles both the mapping of the space and the rendering to the 8 projectors in the dome.

The objective of this internship is to develop a graphical interface that adresses the common use cases of Splash such as live performances with an artist, dome mapping while touring, immersive conference presentations, festivals.

Tasks
-----
With the support of the Metalab team:

* Analyze the use cases
* Suggest one or more interface models
* Develop and evaluate prototypes
* Participate in the lifecycle of the lab: scrums, code reviews, etc.
* Documentation of the work and ensuring its reproducibility

Work environment
----------------
* C++ / Python
* JIRA / Confluence
* GitLab
* Linux, Free and Open Source Software (FOSS)
