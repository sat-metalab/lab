.. title: Facial and body animation / rigging for an immersive experience (LivePose)
.. slug: facial-and-body-rigging-internship
.. date: 2022-02-14
.. tags: internships, livepose
.. category:
.. link:
.. description:
.. type: filled
.. author: Marie-Eve Dumas
.. intern: Éliès Jurquet
.. mentors: Christian Frisson, Emmanuel Durand

Internship title
----------------
Facial and body animation / rigging for an immersive experience (LivePose)

Objective
---------
As part of its research efforts, the SAT's Metalab develops `LivePose <https://gitlab.com/sat-metalab/livepose>`__, an interactive tool for the detection of audience member's positions and poses, using videostreams from live events and installations.

In collaboration with the Metalab team, you will produce a demonstration of real-time capture of participants' facial expressions and body posture and its representation as an avatar in a virtual environment.

A `previous internship <https://sat.qc.ca/fr/nouvelles/workflow-de-capture-et-reproduction-de-visage-dans-blender>`__ on this subject has laid the groundwork for this project.

Tasks
-----
With the support of the Metalab team:

* Experiment with avatars and their skeletons
* Model and edit 3D avatars
* Animate / rig 3D models of face and body
* Participate in the lifecycle of the lab: scrums, code reviews, etc.
* Documentation of the work and ensuring its reproducibility

Work environment
----------------
* 3D modeling, editing, and processing
* Python
* Blender
* JIRA / Confluence
* GitLab
* Linux, Free and Open Source Software (FOSS)
