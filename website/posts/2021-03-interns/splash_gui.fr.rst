.. title: Développement d'une interface graphique pour logiciel de vidéo mapping
.. slug: splash-gui
.. date: 2021-03-22
.. tags: stages
.. category:
.. link:
.. description:
.. type: closed


Titre du stage
--------------
Développement d'une interface graphique pour logiciel de vidéo mapping


Objectif
--------
L'interface graphique est souvent la première couche entre l'utilisateur et le logiciel et détermine si il sera adopté ou non. Ce stage s'intéresse à cette composante essentiel. Il s'agira de revoir l'interface actuelle de Splash, un logiciel de vidéo mapping, et de proposer une interface graphique ciblant à la fois un public débutant et intermédiaire.

Splash est, entre autre, utilisé dans un contexte de haute performance pour les productions de la [SAT] dans la Satosphère. Dans ce contexte, il prend en charge à la fois le mapping de l'espace et le rendu vers les 8 projecteurs habillant le dôme.

L'objectif de ce stage sera de développer une interface graphique pour répondre aux cas d'usage courants d'utilisation de Splash tel que les performances live avec un artiste, le mapping de dôme en situation de tournée, les présentations de conférences immersives, festivals.

Tâches
------
Avec le soutien de l'équipe du Metalab :

* Analyser les cas d'usage
* Proposer un ou plusieurs modèles d'interface
* Développer et évaluer des prototypes
* Participer à la vie du laboratoire : scrums, révision de code, etc.
* Documenter le travail et s'assurer de sa reproductibilité

Environnement de travail
------------------------
* C++ / Python
* JIRA / Confluence
* GitLab
* Linux, Free and Open Source Software (FOSS)
